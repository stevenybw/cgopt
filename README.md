# CGOpt: A tool for optimizing computational graph

## Introduction

With the computational-graph-based machine learning system like TensorFlow and MXNet being popular, we have more opportunities to optimize the graph using graph-transformation and code-generation techniques. This tool offers the user a way to optimize their TensorFlow or MxNet program in production environment.

Currently, CGOpt supports element-wise kernel fusion. We are going to find more and more opportunities in optimizing the computational graph.

## Code Structure

### Element-wise Kernel Generator

  * fusion/pytemplate: A python module offers template-based code generation
  * fusion/template_element_wise.cc: Template for C++ interface of element-wise GPU kernel in tensorflow
  * fusion/template_element_wise.cu: Template for element-wise GPU kernel
  * fusion/element-wise.py: Element-wise kernel generator, which generate corresponding GPU kernel given an element-wise sub-computational-graph
  * fusion/graphutil: A python module offeres representation of Computational Graph.

### TensorFlow Toolkit

  * fusion/tfperfx: A python module offers interface for TensorFlow.

## Sample Run

1. Clone this repo

2. Init submodule:

```
git submodule update --init
```

### Without Element-wise Fusion

```
  1. python run_mul.py
```

### With Element-wise Fusion

```
  1. python run_mul.py --enable_fusion
```

