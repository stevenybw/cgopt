#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/platform/logging.h"

using namespace tensorflow;

REGISTER_OP("/*${op_name}*/")
  /*${op_inputs}*/
  /*${op_outputs}*/
  .SetShapeFn([](::tensorflow::shape_inference::InferenceContext* c) {
      /*${set_shape_fn_set_output}*/
      return Status::OK();
      })
  .Doc(R"doc(generated op for kernel fusion)doc");

void /*${kernel_launcher_name}*/(const int N, /*${arg_list_declaration}*/);

class /*${op_name}*/Op : public OpKernel {
public:
  explicit /*${op_name}*/Op(OpKernelConstruction* context) : OpKernel(context) {}

  void Compute(OpKernelContext* context) override {
    auto shape = context->input(0).shape();
    size_t N = shape.num_elements();

/*${prepare_input_tensor}*/
/*${prepare_output_tensor}*/
/*${prepare_input_flat}*/
/*${prepare_output_flat}*/
/*${prepare_input}*/
/*${prepare_output}*/

    LOG(INFO) << "/*${op_name}*/KernelLaunch: " << N;
    /*${op_name}*/KernelLauncher(N, /*${arg_list}*/);
    LOG(INFO) << "/*${op_name}*/KernelLaunch Done";
  }
};

REGISTER_KERNEL_BUILDER(Name("/*${op_name}*/").Device(DEVICE_GPU), /*${op_name}*/Op);
