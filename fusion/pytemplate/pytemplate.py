import argparse
import re

def convert_camel_snake(name):
  s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
  return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

class PyTemplate:
  def __init__(self, resolve_handler):
    self._resolve_handler = resolve_handler

  def instantiate(self, template_path, output_path):
    with open(output_path, "w") as fp:
      content = open(template_path, 'r').read()
      split_pattern = re.compile(r'(/\*\${[a-zA-Z_][a-zA-Z1-9_]*}\*/)')
      get_var_name = re.compile(r'/\*\${([a-zA-Z_][a-zA-Z1-9_]*)}\*/')
      sp = re.split(split_pattern, content)
      for s in sp:
        M = re.match(get_var_name, s)
        if M:
          resolved = self._resolve_handler(M.group(1))
          fp.write(resolved)
        else:
          fp.write(s)

  def main(self):
    parser = argparse.ArgumentParser(description='PyTemplate: A framework for templating or code generation using python.')
    parser.add_argument('template_path', type=str, 
        help='path of the template to be instantiated')
    parser.add_argument('output_path', type=str, 
        help='the output path of the instance')
    args = parser.parse_args()
    template_path = args.template_path
    output_path = args.output_path
    self.instantiate(template_path, output_path)

