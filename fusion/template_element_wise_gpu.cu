__global__ void /*${kernel_name}*/(const int N, /*${arg_list_declaration}*/) {
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
    /*${kernel_body}*/
  }
}

void /*${kernel_launcher_name}*/(const int N, /*${arg_list_declaration}*/) {
  /*${kernel_name}*/<<<32, 256>>>(N, /*${arg_list}*/);
}


