import tensorflow as tf
import tensorflow.contrib.graph_editor as ge
import time
from graphutil import Graph
from element_wise import ElementWiseCodeGenGraph, convert_camel_snake
import os

element_wise_op = {
    'Add': "{0}+{1}",
    'Mul': "{0}*{1}",
    'Sigmoid': "1.0 / (1.0 + expf(-{0}))",
    'Tanh': "(expf(2*{0})-1.0) / (expf(2*{0}) + 1.0)"
    }

NODE_TFG_OP = "node_tfg_op"
NODE_NAME = "node_name"
NODE_TYPE = "node_type"
NODE_TYPE_ELEMENT_WISE = "ElementWise"
NODE_ELEMENT_WISE_OP = "node_element_wise_op"
NODE_TYPE_INPUT = "Input"
NODE_INPUT_ID = "node_input_id"
NODE_INPUT_TENSOR = "node_input_tensor"
NODE_TYPE_OUTPUT = "Output"
NODE_OUTPUT_ID = "node_output_id"
NODE_OUTPUT_TENSOR = "node_output_tensor"
NODE_DISPLAY = "node_display"

def tfg2graph(tfg):
  ret = Graph()
  """ fetch_tensors: tensor that does not have a consumer yet """
  fetch_tensors = list()
  for op in tfg.get_operations():
    idx = op._id
    name = op.name
    t    = op.type
    device = op.device
    ret.possibly_add_node(idx)
    for i, input_tensor in enumerate(op.inputs):
      src_op = input_tensor.op
      dst_op = op
      src_output = input_tensor.value_index
      dst_input = i
      shape = input_tensor.get_shape().as_list()
      dtype = input_tensor.dtype
      ret.connect(src_op._id, dst_op._id, src_output_index=src_output, dst_input_index=dst_input, tensor_shape=shape, dtype=dtype)
    for output_tensor in op.outputs:
      if len(output_tensor.consumers()) == 0:
        fetch_tensors.append(output_tensor)
    ret.set_attribute(idx, NODE_DISPLAY, name+"@"+t+"@"+device)
    ret.set_attribute(idx, NODE_NAME, name)
    ret.set_attribute(idx, NODE_TFG_OP, op)
  return ret



def op_fusion(tfg, ops):
  ops_set = set(ops)
  graph = tfg2graph(tfg)
  element_wise_only_graph = Graph()
  for op in tfg.get_operations():
    idx = op._id
    name = op.name
    t    = op.type
    device = op.device

    if t in element_wise_op:
      for in_tensor in op.inputs:
        input_idx = in_tensor.op._id
        input_type = in_tensor.op.type
        if input_type in element_wise_op:
          element_wise_only_graph.connect(input_idx, idx)
          element_wise_only_graph.connect(idx, input_idx)
  scc_list = element_wise_only_graph.scc()

  def write_to_dot(graph, name):
    def edge_label(edge):
      return "*".join([str(d) for d in edge.tensor_shape])
    def node_label(node_index):
      return graph.attribute(node_index, NODE_DISPLAY)
    graph.write_to_dot(name, node_label, edge_label)

  write_to_dot(graph, "graph_before_fusion.dot")
  print("scc_list=", scc_list)
  for index, scc in enumerate(scc_list):
    if len(scc) <= 5:
      continue
    scc_set = set(scc)
    sub_graph = Graph()
    for u in scc_set:
      sub_graph.possibly_add_node(u)
      op = graph.attribute(u, NODE_TFG_OP)
      sub_graph.set_attribute(u, NODE_NAME, graph.attribute(u, NODE_NAME))
      sub_graph.set_attribute(u, NODE_DISPLAY, graph.attribute(u, NODE_DISPLAY))
      sub_graph.set_attribute(u, NODE_TYPE, NODE_TYPE_ELEMENT_WISE)
      sub_graph.set_attribute(u, NODE_ELEMENT_WISE_OP, element_wise_op[op.type])
    current_avail_id = max(scc_set) + 10000

    scc_input_edges = list()
    scc_output_edges = list()
    for u in scc_set:
      in_edges = graph.in_edges(u)
      out_edges = graph.out_edges(u)

      for in_edge in in_edges:
        if in_edge.src not in scc_set:
          # Add a new input node
          src = current_avail_id
          current_avail_id += 1
          input_id = len(scc_input_edges)
          scc_input_edges.append(in_edge)
          dst_input = in_edge.dst_input
          tensor_shape = in_edge.tensor_shape
          dtype = in_edge.dtype
          sub_graph.possibly_add_node(src)
          sub_graph.connect(src, u, src_output_index=0, dst_input_index=dst_input, tensor_shape=tensor_shape, dtype=dtype)
          sub_graph.set_attribute(src, NODE_DISPLAY, "input_%d" % input_id)
          sub_graph.set_attribute(src, NODE_NAME, "input_%d" % input_id)
          sub_graph.set_attribute(src, NODE_TYPE, NODE_TYPE_INPUT)
          sub_graph.set_attribute(src, NODE_INPUT_ID, input_id)
        else:
          # Intra edge
          src = in_edge.src
          src_output = in_edge.src_output
          dst_input = in_edge.dst_input
          tensor_shape = in_edge.tensor_shape
          dtype = in_edge.dtype
          sub_graph.connect(src, u, src_output_index=src_output, dst_input_index=dst_input, tensor_shape=tensor_shape, dtype=dtype)

      for out_edge in out_edges:
        if out_edge.dst not in scc_set:
          # Add a new output node
          dst = current_avail_id
          current_avail_id += 1
          output_id = len(scc_output_edges)
          scc_output_edges.append(out_edge)
          src_output = out_edge.src_output
          dst_input = 0
          tensor_shape = in_edge.tensor_shape
          dtype = in_edge.dtype
          sub_graph.possibly_add_node(dst)
          sub_graph.connect(u, dst, src_output_index=src_output, dst_input_index=dst_input, tensor_shape=tensor_shape, dtype=dtype)
          sub_graph.set_attribute(dst, NODE_DISPLAY, "output_%d" % output_id)
          sub_graph.set_attribute(dst, NODE_NAME, "output_%d" % output_id)
          sub_graph.set_attribute(dst, NODE_TYPE, NODE_TYPE_OUTPUT)
          sub_graph.set_attribute(dst, NODE_OUTPUT_ID, output_id)

    kernel_name_camel = "FusionSCC%d" % index
    kernel_name_snake = convert_camel_snake(kernel_name_camel)

    sub_graph.write_to_dot("scc_%d.dot" % index, lambda idx: sub_graph.attribute(idx, NODE_DISPLAY), lambda edge: "*".join([str(d) for d in edge.tensor_shape]))
    cgg = ElementWiseCodeGenGraph(sub_graph, name=kernel_name_camel, dtype="float")
    cgg.generate_kernel()
    os.system("make NAME=%s" % (kernel_name_snake))

    import pdb; pdb.set_trace()
    """ Reconstruct input_tensors, output_tensors from the input_node_ids, output_node_ids """
    input_tensors = [tfg.get_tensor_by_name("%s:%d" % (graph.attribute(e.src, NODE_NAME), e.src_output)) for e in scc_input_edges]
    output_tensors = [tfg.get_tensor_by_name("%s:%d" % (graph.attribute(e.src, NODE_NAME), e.src_output)) for e in scc_output_edges]

    module = tf.load_op_library("./gen/fusion_scc%d_op.so" % (index))
    op_method = getattr(module, kernel_name_snake)
    fusioned_output_tensors = op_method(*input_tensors, name=kernel_name_camel)
    # fusioned_output_tensors = module.fusion_scc0(*input_tensors, name="FusionedOp")
    if isinstance(fusioned_output_tensors, tf.Tensor):
      fusioned_output_tensors = [fusioned_output_tensors]
    if(len(output_tensors) != len(fusioned_output_tensors)):
      raise RuntimeError("The number of original outputs and fusioned outputs mismatch: origin=%d, fusioned=%d" % (len(output_tensors), len(fusioned_output_tensors)))
    print("swap_ts=", ge.swap_ts(output_tensors, fusioned_output_tensors))
      
  graph_after = tfg2graph(tfg)
  write_to_dot(graph_after, "graph_after_fusion.dot")
  return ops

def prune_tfg(tfg, ops):
    pruned_g = tf.Graph()
    ops_select = ge.get_backward_walk_ops(ops, control_inputs=True)
    ge.copy(ge.SubGraphView(ops_select), dst_graph=pruned_g)
    return pruned_g

def op_export_dot(tfg, ops, path, prune=True):
  def write_to_dot(graph, name):
    def edge_label(edge):
      return "*".join([str(d) for d in edge.tensor_shape])
    def node_label(node_index):
      return graph.attribute(node_index, NODE_DISPLAY)
    graph.write_to_dot(name, node_label, edge_label)
  if prune:
    tfg = prune_tfg(tfg, ops)
  operations = tfg.get_operations()
  print(operations)
  our_graph = tfg2graph(tfg)
  write_to_dot(our_graph, path)

def op_benchmark(op, config=None, num_iters=3, name=None):
  with tf.Session(config=config) as sess:
    if name == None:
      name = op.name
    op = tf.group(op)
    init_op = tf.initialize_all_variables()
    bt = time.time()
    sess.run(init_op)
    et = time.time()
    print(name, "init time:", (et - bt) * 1e6, "us");

    bt = time.time()
    result = sess.run(op)
    print("result:", result)
    et = time.time()
    print(name, "first time:", (et - bt) * 1e6, "us");

    bt = time.time()
    NUM_ITERS = num_iters
    for i in range(NUM_ITERS):
      sess.run(op)
    et = time.time()
    print(name, "average time:", (et - bt) / NUM_ITERS * 1e6, "us");

def get_config(optimize = False):
  config = tf.ConfigProto()
  if not optimize:
    config.graph_options.optimizer_options.do_common_subexpression_elimination = False
    config.graph_options.optimizer_options.do_constant_folding = False
    config.graph_options.optimizer_options.do_function_inlining = False
    config.graph_options.optimizer_options.opt_level = -1
  return config
