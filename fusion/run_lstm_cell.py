import tensorflow as tf
import tfperfx as px

num_state = 8*1024

input = tf.random_uniform(shape=[128, num_state], name="input")
c_state = tf.random_uniform(shape=[128, num_state], name="c_state")
m_state = tf.random_uniform(shape=[128, num_state], name="m_state")

cell = tf.contrib.rnn.LSTMCell(num_state)
output, state = cell(input, [c_state, m_state])

embedding = tf.zeros(shape=[num_state, 10], name="embedding")
output = tf.matmul(output, embedding)

px.op_export_dot(output.graph, [output], "lstm_cell.dot")
[output] = px.op_fusion(output.graph, [output])
px.op_benchmark(output, num_iters=20)
px.op_benchmark(output, num_iters=20)
px.op_export_dot(output.graph, [output], "lstm_cell_after.dot")
