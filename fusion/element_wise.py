from graphutil.graph import Graph
import pytemplate
from pytemplate import convert_camel_snake
import re

NODE_TFG_OP = "node_tfg_op"
NODE_NAME = "node_name"
NODE_TYPE = "node_type"
NODE_TYPE_ELEMENT_WISE = "ElementWise"
NODE_ELEMENT_WISE_OP = "node_element_wise_op"
NODE_TYPE_INPUT = "Input"
NODE_INPUT_ID = "node_input_id"
NODE_INPUT_TENSOR = "node_input_tensor"
NODE_TYPE_OUTPUT = "Output"
NODE_OUTPUT_ID = "node_output_id"
NODE_OUTPUT_TENSOR = "node_output_tensor"

def gen_sample_graph():
  g = Graph()
  g.connect(0, 2, src_output_index=0, dst_input_index=0)
  g.connect(1, 2, src_output_index=0, dst_input_index=1)
  g.connect(2, 3, src_output_index=0, dst_input_index=0)
  g.connect(3, 4, src_output_index=0, dst_input_index=0)
  g.connect(5, 4, src_output_index=0, dst_input_index=1)
  g.connect(4, 6, src_output_index=0, dst_input_index=0)
  g.set_attribute(0, NODE_TYPE, "Input")
  g.set_attribute(1, NODE_TYPE, "Input")
  g.set_attribute(5, NODE_TYPE, "Input")
  g.set_attribute(6, NODE_TYPE, "Output")
  g.set_attribute(0, NODE_INPUT_ID, 0)
  g.set_attribute(1, NODE_INPUT_ID, 1)
  g.set_attribute(5, NODE_INPUT_ID, 2)
  g.set_attribute(6, NODE_OUTPUT_ID, 0)
  g.set_attribute(2, NODE_TYPE, "ElementWise")
  g.set_attribute(3, NODE_TYPE, "ElementWise")
  g.set_attribute(4, NODE_TYPE, "ElementWise")
  g.set_attribute(2, NODE_ELEMENT_WISE_OP, "{0}+{1}")
  g.set_attribute(3, NODE_ELEMENT_WISE_OP, "expf({0})")
  g.set_attribute(4, NODE_ELEMENT_WISE_OP, "{0}*{1}")
  return g

def check_graph(g):
  for v in g.nodes():
    t = g.attribute(v, NODE_TYPE)
    if t == "Input":
      in_edges = g.in_edges(v)
      out_edges = g.out_edges(v)
      if not (len(in_edges)==0 and len(out_edges)==1 and out_edges[0].src_output == 0):
        raise RuntimeError("input node %d incorrect: in=%d, out=%d" % (v, len(in_edges), len(out_edges)))
    elif t == "Output":
      in_edges = g.in_edges(v)
      out_edges = g.out_edges(v)
      if not (len(in_edges)==1 and len(out_edges)==0 and in_edges[0].dst_input == 0):
        raise RuntimeError("input node %d incorrect: in=%d, out=%d" % (v, len(in_edges), len(out_edges)))
    elif t == "ElementWise":
      in_edges = g.in_edges(v)
      out_edges = g.out_edges(v)
      ew_op = g.attribute(v, NODE_ELEMENT_WISE_OP)
      num_dependency = len(in_edges)
      num_input = len({int(n) for n in re.findall("\{([0-9]+?)\}", ew_op)})
      if num_dependency != num_input:
        raise RuntimeError("node %d has %d dependencies node, but its op %s has %d input" % (v, num_dependency, ew_op, num_input))
      if {e.dst_input for e in in_edges} != {i for i in range(num_input)}:
        raise RuntimeError("something wrong with node %d<%s>'s input edges: dst_input %s != %s" % (v, ew_op, str({e.dst_input for e in in_edges}), str({i for i in range(num_input)})))


class ElementWiseCodeGenGraph:
  def __init__(self, g, name="Sample", dtype="float"):
    check_graph(g)
    self.name = name
    self.dtype = dtype
    self.op_name = name
    self.kernel_name = name + "Kernel"
    self.kernel_launcher_name = self.kernel_name + "Launcher"
    input_set = set()
    output_set = set()
    input_vertex_to_id = dict()
    output_vertex_to_id = dict()

    def proceed_on_visit_add_input(v):
      if g.attribute(v, NODE_TYPE) == "Input":
        input_set.add(v)
        input_vertex_to_id[v] = g.attribute(v, NODE_INPUT_ID)
      elif g.attribute(v, NODE_TYPE) == "Output":
        output_set.add(v)
        output_vertex_to_id[v] = g.attribute(v, NODE_OUTPUT_ID)
    
    g.dfs(on_visit=proceed_on_visit_add_input)

    num_inputs = len(input_set)
    num_outputs = len(output_set)

    arg_list_decl = (["const %s* input_%d" % (dtype, i) for i in range(num_inputs)] +
           ["%s* output_%d" % (dtype, i) for i in range(num_outputs)])
    self.arg_list_declaration = ", ".join(arg_list_decl)

    arg_list = (["input_%d" % (i) for i in range(num_inputs)] +
           ["output_%d" % (i) for i in range(num_outputs)])
    self.arg_list = ", ".join(arg_list)

    def gen_kernel_body(v):
      assert(v not in output_set)
      if v in input_set:
        return "input_%d[i]" % input_vertex_to_id[v]
      op = g.attribute(v, NODE_ELEMENT_WISE_OP)
      in_nodes = [e.src for e in sorted(g.in_edges(v), key=lambda e: e.dst_input)]
      in_bodies = [gen_kernel_body(i) for i in in_nodes]
      return "(" + op.format(*in_bodies) + ")"

    kernel_bodies = list()
    for v in output_set:
      in_nodes = list(g.in_nodes(v))
      if(len(in_nodes) != 1):
        raise RuntimeError("Output must have exactly one dependency")
      in_node = in_nodes[0]
      body = gen_kernel_body(in_node)
      kernel_bodies.append("output_%d[i] = %s;" % (output_vertex_to_id[v], body))
    self.kernel_body = "\n".join(kernel_bodies)

    self.op_inputs = "".join([".Input(\"input_%d: %s\")" % (i, dtype) 
      for i in range(num_inputs)])

    self.op_outputs = "".join([".Output(\"output_%d: %s\")" % (i, dtype)
      for i in range(num_outputs)])

    self.set_shape_fn_set_output = "".join(["c->set_output(%d, c->input(0));" % (i) for i in range(num_outputs)])

    self.prepare_input_tensor = "\n".join(["const Tensor& input_%d_tensor = context->input(%d);" % (i, i) for i in range(num_inputs)])

    self.prepare_output_tensor = "\n".join(["Tensor* output_%d_tensor = NULL; OP_REQUIRES_OK(context, context->allocate_output(%d, shape, &output_%d_tensor));" 
      % (i, i, i) for i in range(num_outputs)])

    self.prepare_input_flat = "\n".join(["auto input_%d_flat = input_%d_tensor.flat<%s>();" % (i, i, dtype) for i in range(num_inputs)])

    self.prepare_output_flat = "\n".join(["auto output_%d_flat = output_%d_tensor->flat<%s>();" % (i, i, dtype) for i in range(num_outputs)])

    self.prepare_input = "\n".join(["const %s* input_%d = input_%d_flat.data();" % (dtype, i, i) for i in range(num_inputs)])

    self.prepare_output = "\n".join(["%s* output_%d = output_%d_flat.data();" % (dtype, i, i) for i in range(num_outputs)])

  def resolve(self, var):
    return self.__dict__[var]

  def generate_kernel(self):
    pt = pytemplate.PyTemplate(self.resolve)
    pt.instantiate('template_element_wise.cc', "gen/" + pytemplate.convert_camel_snake(self.name) + ".cc")
    pt.instantiate('template_element_wise_gpu.cu', "gen/" + pytemplate.convert_camel_snake(self.name) + "_gpu.cu")

if __name__ == '__main__':
  print("Generating Sample Graph")
  cgg = ElementWiseCodeGenGraph(gen_sample_graph())
  cgg.generate_kernel()
