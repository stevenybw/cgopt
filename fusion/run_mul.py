#!/usr/bin/env python

import tensorflow as tf
import tensorflow.contrib.graph_editor as ge
import tfperfx as px
import time
import argparse

parser = argparse.ArgumentParser(description="Demonstration for element-wise kernel fusion")
parser.add_argument('--enable_fusion', action="store_true")
args = parser.parse_args()

if args.enable_fusion:
  print("Kernel Fusion is enabled")

def benchmark_mul1(X, config=None):
  with tf.variable_scope("mul1"):
    with tf.device("/gpu:0"):
      xi = X[0]
      for i in range(1, len(X)):
        xi = tf.multiply(xi, X[i], name="prod_0_to_%d"%(i))
      xi = tf.identity(xi)

  if args.enable_fusion:
    [xi] = px.op_fusion(tf.get_default_graph(), [xi])

  with tf.device("/gpu:0"):
    op = tf.group(xi, name="benchmark_mul1")
    for i in range(2):
      px.op_benchmark(op, config=config, num_iters=10, name="mul1")
    px.op_export_dot(tf.get_default_graph(), [op], "./mul1.dot")

def off_opt(config):
  config.graph_options.optimizer_options.do_common_subexpression_elimination = False
  config.graph_options.optimizer_options.do_constant_folding = False
  config.graph_options.optimizer_options.do_function_inlining = False
  config.graph_options.optimizer_options.opt_level = -1

def full_opt(config):
  config.graph_options.optimizer_options.global_jit_level = tf.OptimizerOptions.ON_1
  print("JIT ON")

config = tf.ConfigProto()
off_opt(config)
print(config)
shape = [32,1024,1024]

with tf.device("/gpu:0"):
  with tf.variable_scope("single"):
    X = [tf.Variable(tf.ones(shape=shape, dtype=tf.float32), name="x%d" % (idx), dtype=tf.float32) for idx in range(32)]
    
benchmark_mul1(X, config=config)

