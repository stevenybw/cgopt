"""
COMMIT: 6b03b817d51990ff54f87efb892f5cadbf77d42b
Add run_id

From this experiment, we can take the following away:

1. The tensorflow will partition a graph G into many sub-graphs according to devices.
2. If the graph keeps the same, the (id,name,device) of each node will keep unchanged.
3. There is no connection between each subgraph: the only exception is SOURCE and SINK, which is not
   placed into any device.

"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

"""
This class is for storing graph structure
"""

class Graph:
    def __init__(self):
        self._V = set()
        self._name_to_id = dict()
        self._connect = dict()
        self._connect_inv = dict()
        self._attribute = dict()
        self._attribute_pass_2 = dict()
        self._key_set = set()
        self._key_to_node_ids = dict()
        self._has_cycle = False
        self._has_done_dfs = False

    def in_nodes(self, node_id: int) -> set:
        return self._connect_inv[node_id]

    def out_nodes(self, node_id: int) -> set:
        return self._connect[node_id]

    def _possibly_add_node(self, u):
        if u not in self._V:
            self._V.add(u)
            self._connect[u] = set()
            self._connect_inv[u] = set()
            val = dict()
            val['in_degree'] = 0
            val['out_degree'] = 0
            self._attribute[u] = val

    def delete(self, node_id):
        assert(node_id in self._V)
        dependencies = self.in_nodes(node_id)
        succeeding = self.out_nodes(node_id)

        for u in dependencies:
            assert node_id in self.out_nodes(u)
            self.out_nodes(u).remove(node_id)

        for v in succeeding:
            assert node_id in self.in_nodes(v)
            self.in_nodes(v).remove(node_id)

        del self._connect[node_id]
        del self._connect_inv[node_id]
        del self._attribute[node_id]
        del self._attribute_pass_2[node_id]

        self._V.remove(node_id)

    def connect(self, u, v):
        self._possibly_add_node(u)
        self._possibly_add_node(v)
        if v not in self._connect[u]:
            self._connect[u].add(v)
            self._connect_inv[v].add(u)
            self._attribute[u]['out_degree'] += 1
            self._attribute[v]['in_degree'] += 1

    def set_attribute(self, node_id, name, value):
        self._attribute[node_id][name] = value

    def set_attribute_unique(self, node_id, name, value):
        if name in self._attribute[node_id]:
            assert (self._attribute[node_id][name] == value)
        else:
            self._attribute[node_id][name] = value

    def attribute(self, node_id, name):
        return self._attribute[node_id][name]

    def set_node_name(self, node_id, node_name):
        if node_name not in self._name_to_id:
            self._name_to_id[node_name] = node_id
        else:
            assert (self._name_to_id[node_name] == node_id)

        self.set_attribute_unique(node_id, "node_name", node_name)

    def get_node_name(self, node_id):
        return self.attribute(node_id, "node_name")

    def set_node_device(self, node_id, value):
        self.set_attribute_unique(node_id, "node_device", value)

    def get_node_device(self, node_id):
        return self.attribute(node_id, "node_device")

    def set_node_type(self, node_id, value):
        self.set_attribute_unique(node_id, "node_type", value)

    def get_node_type(self, node_id):
        return self.attribute(node_id, "node_type")

    def set_node_key(self, node_id, key):
        assert(self.is_send_recv(node_id))
        self.set_attribute_unique(node_id, "node_key", key)

    def get_node_key(self, node_id):
        assert (self.is_send_recv(node_id))
        return self.attribute(node_id, "node_key")

    def get_node_partner_id(self, node_id):
        assert (self.is_send_recv(node_id))
        return self.attribute(node_id, "partner_id")

    def get_node_scheduled_time(self, node_id):
        return self._attribute_pass_2[node_id]["last_scheduled_timestamp"]

    def get_node_finished_time(self, node_id):
        return self._attribute_pass_2[node_id]["last_finished_timestamp"]

    def get_node_duration(self, node_id):
        return self.get_node_finished_time(node_id) - self.get_node_scheduled_time(node_id)

    def is_send_recv(self, node_id):
        return self.get_node_type(node_id) in {"_Send", "_Recv", "_HostSend", "_HostRecv"}

    def is_send(self, node_id):
        return self.get_node_type(node_id) in {'_Send', '_HostSend'}

    def is_recv(self, node_id):
        return self.get_node_type(node_id) in {'_Recv', '_HostRecv'}

    def is_cyclic(self):
        if not self._has_done_dfs:
            self.dfs()
            self._has_done_dfs = True
        return self._has_cycle

    def num_nodes(self):
        return len(self._V)

    def dfs_helper(self, source):
        if source not in self._connect:
            return
        for v in self._connect[source]:
            if self.state[v] == 0:
                self.state[v] = 1
                self.dfs_helper(v)
                self.state[v] = 2
            elif self.state[v] == 1:
                self._has_cycle = True

    def dfs(self):
        N = self.num_nodes()
        self.state = dict()
        for k in self._V:
            self.state[k] = 0
        for u in self._V:
            if self.state[u] == 0:
                self.state[u] = 1
                self.dfs_helper(u)
                self.state[u] = 2
            elif self.state[u] == 1:
                self._has_cycle = True


def ASSERT_EQ(x, y, extra):
    if x != y:
        print(extra, ": ASSERTION FAILED: ", x, " != ", y)
        raise RuntimeError()


def reduce_sum(kv_pairs):
    acc = dict()
    for kv in kv_pairs:
        k = kv[0]
        v = kv[1]
        val = acc.get(k, 0.0)
        acc[k] = val + v
    return acc


def graph_equal(g_A, g_B):
    if len(g_A._V) != len(g_B._V):
        print("0")
        return False
    A_name = g_A._name_to_id.keys()
    B_name = g_B._name_to_id.keys()

    for u in A_name:
        if u not in B_name:
            print(u, "in A but is not in B")
            return False

    for u in B_name:
        if u not in A_name:
            print("2")
            return False

    for u in g_A._V:
        if u not in g_B._V:
            name_A = g_A._attribute[u]['node_name']
            print(u, "in A but is not in B, while", name_A, "is in", g_B._name_to_id[name_A])
            return False

    for u in g_B._V:
        if u not in g_A._V:
            print(u, "in B but is not in A")
            return False

    for u in g_A._V:
        name_A = g_A._attribute[u]['node_name']
        name_B = g_B._attribute[u]['node_name']
        type_A = g_A._attribute[u]['node_type']
        type_B = g_B._attribute[u]['node_type']
        if name_A != name_B:
            print(u, "mismatch:", name_A, g_B._name_to_id[name_A], name_B, g_A._name_to_id[name_B])
            return False

        if type_A != type_B:
            print("4")
            return False

    return True


TRACE_TYPE_MISC_BEGIN = 1
TRACE_TYPE_MISC_END = 2
TRACE_TYPE_SENDRECV_BEGIN = 3
TRACE_TYPE_SENDRECV_END = 4
TRACE_TYPE_NEW_EXECUTORS_AND_KEYS = 5

EVENT_BEGIN = 1
EVENT_END   = 2


class Traces():
    def __init__(self):
        self._traces = list()

    def insert(self, step_id, timestamp, event_type, node_id):
        self._traces.append({"event_type": event_type, "node_id": node_id, "step_id": step_id, "timestamp": timestamp})

    def as_list(self) -> list:
        return self._traces


class ExecutorsAndKeys:
    def __init__(self):
        self._target_node_names = set()
        self._step_ids = set()

    def add_target_node(self, name):
        self._target_node_names.add(name)

    def add_step_id(self, step_id):
        self._step_ids.add(step_id)

    def target_nodes(self):
        return self._target_node_names

    def step_ids(self):
        return self._step_ids


class TraceReader():
    """
    We have gained the fact that two distinct nodes' id may be the same, because the
    graph has been partitioned by devices and the id is the id that is inside the device.

    Methods:
        load: load the file
        graph: get the graph


    Attributes
         step_id_trace_count:  the number of traces of each step
         step_id_to_traces:    get the traces of specific step_id
         step_id_to_graph:     get the graph of specific step_id



    """

    def __init__(self, step_filter):
        self.step_id_set = set()
        self.step_id_trace_count = np.zeros(30000)
        self.step_id_earlist_timestamp = np.zeros(30000)
        self.step_id_latest_timestamp = np.zeros(30000)
        self.device_id_range = 10000
        self.device_id_counter = 0
        self.device_name_to_id = dict()
        self.device_id_to_name = dict()
        self.step_filter = step_filter
        self.step_id_to_traces = dict()
        self.step_id_to_graph = dict()
        self.run_id_to_ek = dict()
        self._loaded = False

    def graph(self, step_id: int) -> Graph:
        return self.step_id_to_graph[step_id]

    def traces(self, step_id: int) -> Traces:
        return self.step_id_to_traces[step_id]

    def ek(self, run_id: int) -> ExecutorsAndKeys:
        if run_id not in self.run_id_to_ek:
            self.run_id_to_ek[run_id] = ExecutorsAndKeys()
        return self.run_id_to_ek[run_id]

    def get_device_offset(self, device_name):
        if device_name in self.device_name_to_id:
            return self.device_name_to_id[device_name] * self.device_id_range
        else:
            assigned_id = self.device_id_counter
            self.device_id_counter += 1
            print("Assign device ", device_name, " to offset ", assigned_id)
            self.device_id_to_name[assigned_id] = device_name
            self.device_name_to_id[device_name] = assigned_id
            return self.device_name_to_id[device_name] * self.device_id_range

    def maintains_step_id(self, step_id, timestamp):
        if step_id > 0:
            self.step_id_trace_count[step_id] += 1
            if self.step_id_earlist_timestamp[step_id] == 0:
                self.step_id_earlist_timestamp[step_id] = timestamp
                self.step_id_latest_timestamp[step_id] = timestamp
            else:
                self.step_id_earlist_timestamp[step_id] = min(timestamp, self.step_id_earlist_timestamp[step_id])
                self.step_id_latest_timestamp[step_id] = max(timestamp, self.step_id_latest_timestamp[step_id])

        if self.step_filter(step_id) and step_id not in self.step_id_set:
            self.step_id_set.add(step_id)
            self.step_id_to_graph[step_id] = Graph()
            self.step_id_to_traces[step_id] = Traces()

    def _pass_3(self):
        """
        Complete partner_id information for each communication node
        make key_set and key_to_node_ids available.
        """

        for step_id in self.step_id_set:
            graph = self.graph(step_id)

            key_set = graph._key_set
            key_to_node_ids = graph._key_to_node_ids

            for u in graph._V:
                if graph.is_send_recv(u):
                    key = graph.get_node_key(u)
                    if key not in key_set:
                        key_set.add(key)
                        key_to_node_ids[key] = set()
                    key_to_node_ids[key].add(u)

            for k in key_set:
                node_ids = list(key_to_node_ids[k])
                if len(node_ids) == 2:
                    if graph.is_send(node_ids[0]) and graph.is_recv(node_ids[1]):
                        send_id = node_ids[0]
                        recv_id = node_ids[1]
                    elif graph.is_recv(node_ids[0]) and graph.is_send(node_ids[1]):
                        send_id = node_ids[1]
                        recv_id = node_ids[0]
                    else:
                        continue

                    graph.set_attribute_unique(recv_id, "partner_id", send_id)
                    graph.set_attribute_unique(send_id, "partner_id", recv_id)

    def _parse_line_pass_1(self, line: str) -> None:
        """The First Pass of Trace Analysis
        The First Pass of Trace Analysis extract static information like the
        structure of the graph, each node's name, device, type. And maintains
        info of each step.
        Traces of each step_id will be stored, and can be accessed by traces
        method.

        Args:
            line:

        Returns:

        """
        sp = line.split(",")
        trace_type = int(sp[0])
        if trace_type == TRACE_TYPE_MISC_BEGIN:
            assert (len(sp) == 9)
            subgraph_node_id = int(sp[1])
            step_id = int(sp[2])
            node_name = sp[3]
            node_type = sp[4]
            node_device = sp[5]
            timestamp = float(sp[6])
            in_node_id_list = sp[7]

            # Process the run_id
            # if step_id == -1, its run_id may be zero, which is invalid.
            run_id = int(sp[8])
            if step_id > 0:
                if run_id not in self.run_id_to_ek:
                    print(line)
                assert(run_id in self.run_id_to_ek)
                self.ek(run_id).add_step_id(step_id)

            self.maintains_step_id(step_id, timestamp)
            if not self.step_filter(step_id):
                return
            g = self.graph(step_id)
            traces = self.traces(step_id)
            node_id = subgraph_node_id + self.get_device_offset(node_device)
            in_node_id_split = in_node_id_list.split()[1:-1]
            g._possibly_add_node(node_id)
            g.set_node_name(node_id, node_name)
            g.set_node_type(node_id, node_type)
            g.set_node_device(node_id, node_device)
            for identifier in in_node_id_split:
                target_sp = identifier.split("!")
                assert (len(target_sp) == 3)
                target_subgraph_id = int(target_sp[0])
                target_name = target_sp[1]
                target_device = target_sp[2]

                """ This guarantee that there is only one device in each strongly-connected component """
                if target_device != "" and node_device != "":
                    ASSERT_EQ(target_device, node_device, "(%s %s)" % (target_name, node_name))

                target_id = target_subgraph_id + self.get_device_offset(target_device)
                if target_id in g._V and "node_name" in g._attribute[target_id]:
                    ASSERT_EQ(g._attribute[target_id]["node_name"], target_name, "TARGET")
                g.connect(target_id, node_id)
            traces.insert(step_id, timestamp, EVENT_BEGIN, node_id)

        elif trace_type == TRACE_TYPE_SENDRECV_BEGIN:
            assert (len(sp) == 10)
            subgraph_node_id = int(sp[1])
            step_id = int(sp[2])
            node_name = sp[3]
            node_type = sp[4]
            node_device = sp[5]
            timestamp = float(sp[6])
            in_node_id_list = sp[7]
            key = sp[8]

            # Process the run_id
            run_id = int(sp[9])
            if step_id > 0:
                if run_id not in self.run_id_to_ek:
                    print(line)
                assert(run_id in self.run_id_to_ek)
                self.ek(run_id).add_step_id(step_id)


            self.maintains_step_id(step_id, timestamp)
            if not self.step_filter(step_id):
                return
            g = self.graph(step_id)
            traces = self.traces(step_id)
            node_id = subgraph_node_id + self.get_device_offset(node_device)
            in_node_id_split = in_node_id_list.split()[1:-1]
            g._possibly_add_node(node_id)
            g.set_node_name(node_id, node_name)
            g.set_node_type(node_id, node_type)
            g.set_node_device(node_id, node_device)
            g.set_node_key(node_id, key)
            for identifier in in_node_id_split:
                target_sp = identifier.split("!")
                assert (len(target_sp) == 3)
                target_subgraph_id = int(target_sp[0])
                target_name = target_sp[1]
                target_device = target_sp[2]

                """ This guarantee that there is only one device in each strongly-connected component """
                if target_device != "" and node_device != "":
                    ASSERT_EQ(target_device, node_device, "(%s %s)" % (target_name, node_name))

                target_id = target_subgraph_id + self.get_device_offset(target_device)
                if target_id in g._V and "node_name" in g._attribute[target_id]:
                    ASSERT_EQ(g._attribute[target_id]["node_name"], target_name, "TARGET")
                g.connect(target_id, node_id)
            traces.insert(step_id, timestamp, EVENT_BEGIN, node_id)
        elif trace_type == TRACE_TYPE_MISC_END or trace_type == TRACE_TYPE_SENDRECV_END:
            assert(len(sp) == 5)
            subgraph_node_id = int(sp[1])
            step_id = int(sp[2])
            node_device = sp[3]
            timestamp = float(sp[4])

            self.maintains_step_id(step_id, timestamp)
            if not self.step_filter(step_id):
                return
            g = self.graph(step_id)
            traces = self.traces(step_id)
            node_id = subgraph_node_id + self.get_device_offset(node_device)
            traces.insert(step_id, timestamp, EVENT_END, node_id)
        elif trace_type == TRACE_TYPE_NEW_EXECUTORS_AND_KEYS:
            assert(len(sp) == 3)
            run_id = int(sp[1])
            tn_list = sp[2]

            ek = self.ek(run_id)
            for target_node in tn_list.split()[1:-1]:
                ek.add_target_node(target_node)
        else:
            assert(False)


    def _pass_2(self):
        for step_id in self.step_id_set:
            graph = self.graph(step_id)
            traces = self.traces(step_id)
            STATE_IDLE = 1
            STATE_SCHEDULED = 2
            TRACE_TYPE_BEGIN = 1
            TRACE_TYPE_END = 2

            traces_sorted = sorted(traces.as_list(), key=lambda x: x['timestamp'])
            for u in graph._V:
                val = dict()
                val['scheduled_count'] = 0
                val['state'] = STATE_IDLE
                val['last_scheduled_timestamp'] = 0.0
                val['last_finished_timestamp'] = 0.0
                val['total_scheduled_time'] = 0.0
                val['scheduler_hole'] = 0.0
                graph._attribute_pass_2[u] = val

            for t in traces_sorted:
                event_type = t['event_type']
                node_id = t['node_id']
                trace_step_id = t['step_id']
                assert(trace_step_id == step_id)
                timestamp = t['timestamp']
                assert (graph._attribute[0]['node_name'] == '_SOURCE')
                assert (graph._attribute[1]['node_name'] == '_SINK')

                # _SOURCE
                if node_id == 0:
                    if graph._attribute_pass_2[node_id]['scheduled_count'] == 0:
                        graph._attribute_pass_2[node_id]['scheduled_count'] = 1
                        if (event_type == TRACE_TYPE_BEGIN):
                            graph._attribute_pass_2[node_id]['last_scheduled_timestamp'] = timestamp
                        elif (event_type == TRACE_TYPE_END):
                            graph._attribute_pass_2[node_id]['last_finished_timestamp'] = timestamp
                            graph._attribute_pass_2[node_id]['total_scheduled_time'] = graph._attribute_pass_2[node_id][
                                                                                     'last_finished_timestamp'] - \
                                                                                       graph._attribute_pass_2[node_id][
                                                                                     'last_scheduled_timestamp']
                    else:
                        if (event_type == TRACE_TYPE_BEGIN):
                            graph._attribute_pass_2[node_id]['last_scheduled_timestamp'] = min(
                                graph._attribute_pass_2[node_id]['last_scheduled_timestamp'], timestamp)
                        elif (event_type == TRACE_TYPE_END):
                            graph._attribute_pass_2[node_id]['last_finished_timestamp'] = min(
                                graph._attribute_pass_2[node_id]['last_scheduled_timestamp'], timestamp)
                            graph._attribute_pass_2[node_id]['total_scheduled_time'] = graph._attribute_pass_2[node_id][
                                                                                     'last_finished_timestamp'] - \
                                                                                       graph._attribute_pass_2[node_id][
                                                                                     'last_scheduled_timestamp']
                    continue

                # _SINK
                elif node_id == 1:
                    if graph._attribute_pass_2[node_id]['scheduled_count'] == 0:
                        graph._attribute_pass_2[node_id]['scheduled_count'] = 1
                        if (event_type == TRACE_TYPE_BEGIN):
                            graph._attribute_pass_2[node_id]['last_scheduled_timestamp'] = timestamp
                        elif (event_type == TRACE_TYPE_END):
                            graph._attribute_pass_2[node_id]['last_finished_timestamp'] = timestamp
                            graph._attribute_pass_2[node_id]['total_scheduled_time'] = graph._attribute_pass_2[node_id][
                                                                                     'last_finished_timestamp'] - \
                                                                                       graph._attribute_pass_2[node_id][
                                                                                     'last_scheduled_timestamp']
                    else:
                        if (event_type == TRACE_TYPE_BEGIN):
                            graph._attribute_pass_2[node_id]['last_scheduled_timestamp'] = max(
                                graph._attribute_pass_2[node_id]['last_scheduled_timestamp'], timestamp)
                        elif (event_type == TRACE_TYPE_END):
                            graph._attribute_pass_2[node_id]['last_finished_timestamp'] = max(
                                graph._attribute_pass_2[node_id]['last_scheduled_timestamp'], timestamp)
                            graph._attribute_pass_2[node_id]['total_scheduled_time'] = graph._attribute_pass_2[node_id][
                                                                                     'last_finished_timestamp'] - \
                                                                                       graph._attribute_pass_2[node_id][
                                                                                     'last_scheduled_timestamp']
                    continue

                if (event_type == TRACE_TYPE_BEGIN):
                    assert (graph._attribute_pass_2[node_id]['state'] == STATE_IDLE)
                    graph._attribute_pass_2[node_id]['state'] = STATE_SCHEDULED
                    graph._attribute_pass_2[node_id]['scheduled_count'] += 1
                    graph._attribute_pass_2[node_id]['last_scheduled_timestamp'] = timestamp
                elif (event_type == TRACE_TYPE_END):
                    assert (graph._attribute_pass_2[node_id]['state'] == STATE_SCHEDULED)
                    graph._attribute_pass_2[node_id]['state'] = STATE_IDLE
                    scheduled_time = timestamp - graph._attribute_pass_2[node_id]['last_scheduled_timestamp']
                    # graph._simulation[node_id]['last_scheduled_timestamp'] = 0
                    graph._attribute_pass_2[node_id]['last_finished_timestamp'] = timestamp
                    graph._attribute_pass_2[node_id]['total_scheduled_time'] += scheduled_time

            for u in graph._V:
                if u != 0:
                    # ready_time = max([graph._simulation[v]['last_finished_timestamp'] for v in graph._connect_inv[u]])
                    ready_time = max([graph._attribute_pass_2[v]['last_finished_timestamp'] for v in graph._connect_inv[u] if
                                      graph._attribute_pass_2[v]['last_finished_timestamp'] < graph._attribute_pass_2[u][
                                          'last_finished_timestamp']])
                    graph._attribute_pass_2[u]['scheduler_hole'] = graph._attribute_pass_2[u]['last_scheduled_timestamp'] - ready_time

    def _pass_1(self, trace_file):
        with open(trace_file, "r") as fp:
            lines = fp.readlines()
            for line in lines:
                self._parse_line_pass_1(line)

    def load(self, trace_file):
        assert (not self._loaded)
        self._loaded = True
        self._pass_1(trace_file)

        """ ASSERTIONS """
        assert (self.step_id_to_traces.keys() == self.step_id_to_graph.keys())
        step_ids = sorted(self.step_id_to_traces.keys())
        print("[LOAD] Captured Step Ids =", ", ".join([str(v) for v in step_ids]))
        g_left = self.step_id_to_graph[step_ids[0]]
        for step_id in step_ids[1:]:
            g_right = self.step_id_to_graph[step_id]
            assert (graph_equal(g_left, g_right))
        print("[LOAD] Graph equality has been verified")

        self._pass_2()
        self._pass_3()
        print("[LOAD] Graph Analyzed")


    def show_duration_per_step_id(self):
        print(np.sum(self.step_id_latest_timestamp - self.step_id_earlist_timestamp))
        Last = last_index(self.step_id_trace_count)
        Duration = self.step_id_latest_timestamp - self.step_id_earlist_timestamp
        plt.plot(Duration[:Last])
        plt.xlabel("StepId")
        plt.ylabel("Duration (us)")
        plt.yscale('log')
        plt.show()

    def show_runtime_num_traces_per_step(self):
        # plt.figure(figsize=(15,5))
        plt.yscale('symlog')
        plt.xlabel('StepID')
        plt.ylabel('Number of Traces')
        Last = last_index(self.step_id_trace_count)
        plt.plot(self.step_id_trace_count[:Last])
        plt.show()


class Analysis:
    def __init__(self, reader: TraceReader) -> None:
        self._reader  = reader
        self.graph_fx = reader.graph
        self.traces_fx = reader.traces
        self.device_name_to_id_dict = reader.device_name_to_id

    def graph(self, step_id: int) -> Graph:
        return self.graph_fx(step_id)

    def traces(self, step_id: int) -> Traces:
        return self.traces_fx(step_id)

    def device_name_to_id(self, device_name: str) -> int:
        return self.device_name_to_id_dict[device_name]

    def step_id_duration(self, step_id: int) -> float:
        return self._reader.step_id_latest_timestamp[step_id] - self._reader.step_id_earlist_timestamp[step_id]

    def key_set(self, step_id):
        return self.graph(step_id)._key_set

    def key_to_node_ids(self, step_id):
        return self.graph(step_id)._key_to_node_ids

    """ Whole Program """
    def program_running_time_us(self):
        return max(self._reader.step_id_latest_timestamp) - min(self._reader.step_id_earlist_timestamp)

    """ For each task"""
    def show_tasks(self):
        for task_id in self._reader.run_id_to_ek:
            ek = self._reader.ek(task_id)
            num_steps = len(ek.step_ids())
            target_nodes = ek.target_nodes()
            step_durations = [self.step_id_duration(step_id) for step_id in ek.step_ids()]
            duration_average = np.average(step_durations)
            duration_std_variance = np.sqrt(np.var(step_durations))
            print(target_nodes, "count =",num_steps, "average_time =", duration_average,  "variance =", duration_std_variance)


    """ Basic Graph Features """
    def show_graph_node_degrees_distribution(self, step_id):
        g = self.graph(step_id)
        print("Vertex Out-Degree:")
        out_degrees = [g._attribute[u]['out_degree'] for u in g._V]
        in_degrees = [g._attribute[u]['in_degree'] for u in g._V]
        plt.hist(out_degrees, bins=50)
        plt.xlabel("Out Degree")
        plt.ylabel("Count")
        plt.yscale('symlog')
        plt.show()
        plt.hist(in_degrees, bins=50)
        plt.xlabel("Out Degree")
        plt.ylabel("Count")
        plt.yscale('symlog')
        plt.show()

    def show_graph_num_nodes_per_device(self, step_id):
        g = self.graph(step_id)
        devices = [(g._attribute[u]['node_device'], 1) for u in g._V]
        X = reduce_sum(devices)
        K = [str(k)[-5:] for k in X.keys()]
        V = [X[k] for k in X.keys()]
        plt.figure(figsize=(6, 6))
        plt.pie(V, labels=K)
        plt.title("Number of Nodes per Device")
        plt.show()

    def show_graph_num_nodes_per_type(self, step_id):
        g = self.graph(step_id)
        node_types = [(g._attribute[u]['node_type'], 1) for u in g._V]
        X = reduce_sum(node_types)
        K = sorted(X.keys(), key=lambda k: X[k], reverse=True)
        V = [X[k] for k in K]
        plt.figure(figsize=(10, 5))
        plt.yscale("log")
        plt.ylabel('Number of Nodes')
        plt.bar(range(len(K)), V)
        ax = plt.gca()
        ax.set_xticks(range(len(K)))
        ax.set_xticklabels(K, rotation=90, ha='left')
        plt.title("Number of Nodes per Node Type")
        plt.show()

    def show_static_analysis(self, step_id):
        g = self.graph(step_id)
        if g.is_cyclic():
            print("[WARNING] There is a cycle in the graph!")
        else:
            print("[OK] This graph is indeed a DAG")
        print("num_nodes =", len(g._V))

    def show_scheduler_hole_per_node(self, step_id):
        graph = self.graph(step_id)
        ID_list = sorted(graph._V)
        Hole_list = [graph._attribute_pass_2[u]["scheduler_hole"] for u in ID_list]
        ID_illegal = [u for u in ID_list if graph._attribute_pass_2[u]["scheduler_hole"] < 0.0]
        plt.figure(figsize=(15, 5))
        # plt.yscale('symlog')
        plt.xlabel('Node ID')
        plt.ylabel('Scheduler Hole (us)')
        plt.plot(Hole_list)
        plt.show()
        print(ID_illegal)
        for u in ID_illegal:
            # print(u, graph._attribute[u]['node_name'], graph._attribute[u]['node_type'], graph.attribute_pass_2[u]['last_scheduled_timestamp'])
            # print([(v, graph._attribute[v]['node_name'], graph._attribute[v]['node_type'], graph.attribute_pass_2[v]['last_finished_timestamp']) for v in graph._connect_inv[u]])
            print(u, graph._attribute[u]['node_type'], graph._attribute_pass_2[u]['last_scheduled_timestamp'])
            print([(v, graph._attribute[v]['node_type'], graph._attribute_pass_2[v]['last_finished_timestamp']) for v in
                   graph._connect_inv[u]])
            print()

    def get_critical_path(self, step_id):
        graph = self.graph(step_id)
        critical_path = list()
        u = 1
        while u != 0:
            critical_path.append(u)
            latest_dependency_timestamp, latest_dependency = max(
                [(graph._attribute_pass_2[v]['last_finished_timestamp'], v) for v in graph._connect_inv[u] if
                 graph._attribute_pass_2[v]['last_finished_timestamp'] < graph._attribute_pass_2[u]['last_finished_timestamp']])
            if graph.is_recv(u):
                """ Decide corresponding _Send, or dependency is the bottleneck """
                recv_id = u
                send_id = graph.get_node_partner_id(recv_id)
                ts1 = graph.get_node_scheduled_time(send_id)
                ts2 = graph.get_node_finished_time(send_id)
                tr1 = graph.get_node_scheduled_time(recv_id)
                tr2 = graph.get_node_finished_time(recv_id)
                if(ts2 < tr1):
                    """ _Recv arrived too late """
                    u = latest_dependency
                else:
                    """ _Send arrived too late """
                    u = send_id
            else:
                u = latest_dependency
        critical_path.append(u)
        return critical_path

    def get_critical_path_with_time(self, step_id):
        graph = self.graph(step_id)
        critical_path = list()
        u = max([(graph.get_node_finished_time(u), u) for u in graph._V])[1]
        #u = 1
        INF = 1e20
        last_recv_end = INF
        while u != 0:
            latest_dependency_timestamp, latest_dependency = max(
                [(graph._attribute_pass_2[v]['last_finished_timestamp'], v) for v in graph._connect_inv[u] if
                 graph._attribute_pass_2[v]['last_finished_timestamp'] < graph._attribute_pass_2[u]['last_finished_timestamp']])
            if graph.is_recv(u):
                """ Decide corresponding _Send, or dependency is the bottleneck """
                recv_id = u
                send_id = graph.get_node_partner_id(recv_id)
                ts1 = graph.get_node_scheduled_time(send_id)
                ts2 = graph.get_node_finished_time(send_id)
                tr1 = graph.get_node_scheduled_time(recv_id)
                tr2 = graph.get_node_finished_time(recv_id)
                if(ts1 < tr1):
                    """ _Recv begins too late """
                    critical_path.append((u, graph.get_node_scheduled_time(u), graph.get_node_finished_time(u)))
                    u = latest_dependency
                else:
                    """ _Send arrived too late """
                    last_recv_end = tr2
                    u = send_id
            elif graph.is_send(u) and last_recv_end != INF:
                # This is because recv's end will always be after send's begin
                assert(graph.get_node_scheduled_time(u) < last_recv_end)
                critical_path.append((u, graph.get_node_scheduled_time(u), min(graph.get_node_finished_time(u), last_recv_end)))
                last_recv_end = INF
                u = latest_dependency
            else:
                critical_path.append((u, graph.get_node_scheduled_time(u), graph.get_node_finished_time(u)))
                u = latest_dependency
        critical_path.append((u, graph.get_node_scheduled_time(u), graph.get_node_finished_time(u)))
        return critical_path

    class SumStatistic:
        def __init__(self):
            self._stat = dict()
        def add(self, name, value):
            if name not in self._stat:
                self._stat[name] = 0.0
            self._stat[name] += value

        def show_as_bar(self, ylabel="Y-Label", yscale="log", title=None):
            K = sorted(self._stat.keys(), key=lambda k: self._stat[k], reverse=True)
            V = [self._stat[k] for k in K]
            plt.figure(figsize=(10, 5))
            plt.yscale(yscale)
            plt.ylabel(ylabel)
            plt.bar(range(len(K)), V)
            ax = plt.gca()
            ax.set_xticks(range(len(K)))
            ax.set_xticklabels(K, rotation=90, ha='left')
            if title:
                plt.title(title)
            plt.show()

        def show_as_pie(self, title=None):
            K = sorted(self._stat.keys(), key=lambda k: self._stat[k], reverse=True)
            V = [self._stat[k] for k in K]
            plt.figure(figsize=(10, 10))
            plt.pie(V, labels=K)
            if title:
                plt.title(title)
            plt.show()

        def total_sum(self):
            sumval = 0.0
            for k in self._stat:
                sumval += self._stat[k]
            return sumval

    def show_critical_path_node_type(self, step_id):
        graph = self.graph(step_id)
        critical_path = list(reversed(self.get_critical_path_with_time(step_id)))
        assert(critical_path[0][0] == 0)
        stat = self.SumStatistic()
        last_op_time = graph.get_node_finished_time(0)
        empty_time = 0.0
        for item in critical_path[1:]:
            u = item[0]
            t1 = item[1]
            t2 = item[2]
            if graph.is_send_recv(u):
                node_type = "COMM"
            else:
                node_type = graph.get_node_type(u)
            stat.add(node_type, t2 - t1)
            assert(last_op_time <= t1)
            empty_time += t1 - last_op_time
            last_op_time = t2
        stat.add("SHOLE", empty_time)
        print("Sum of Critical Path Time =", stat.total_sum())
        print("Actual Step Duration =", self.step_id_duration(step_id))
        stat.show_as_bar("time (us)", yscale="symlog",title="Time Count v.s Type in a Critical Path")


    def show_critical_path_node_device(self, step_id):
        graph = self.graph(step_id)
        critical_path = list(reversed(self.get_critical_path_with_time(step_id)))
        assert(critical_path[0][0] == 0)
        stat = self.SumStatistic()
        last_op_time = graph.get_node_finished_time(0)
        empty_time = 0.0
        for item in critical_path[1:]:
            u = item[0]
            t1 = item[1]
            t2 = item[2]
            if graph.is_send_recv(u):
                node_device = "COMM"
            else:
                node_device = graph.get_node_device(u)
            stat.add(node_device, t2 - t1)
            assert(last_op_time <= t1)
            empty_time += t1 - last_op_time
            last_op_time = t2
        stat.add("SHOLE", empty_time)
        print("Sum of Critical Path Time =", stat.total_sum())
        print("Actual Step Duration =", self.step_id_duration(step_id))
        stat.show_as_pie(title="Time Count v.s Device in a Critical Path")

    def get_scheduler_hole(self, step_id):
        graph = self.graph(step_id)
        critical_path = list(reversed(self.get_critical_path_with_time(step_id)))
        assert(critical_path[0][0] == 0)
        scheduler_hole = list()
        last_op_time = graph.get_node_finished_time(0)
        for item in critical_path[1:]:
            u = item[0]
            t1 = item[1]
            t2 = item[2]
            if graph.is_send_recv(u):
                node_device = "COMM"
            else:
                node_device = graph.get_node_device(u)
            assert(last_op_time <= t1)
            scheduler_hole.append((last_op_time, t1, u))
            last_op_time = t2
        return scheduler_hole

    def get_critical_path_scheduler_hole(self, step_id):
        graph = self.graph(step_id)
        critical_path = self.get_critical_path(step_id)

        s = 0.0
        for u in critical_path:
            s += graph._attribute_pass_2[u]['scheduler_hole']
        return s

    def is_all_nodes_execution_once(self, step_id):
        graph = self.graph(step_id)
        for u in graph._V:
            if graph._attribute_pass_2[u]['scheduled_count'] != 1:
                print("[WARNING] NOT All of the nodes except _SINK and _SOURCE are executed exactly once.")
                return False
        print("[INFO] All of the nodes except _SINK and _SOURCE are executed exactly once.")
        return True

    def plot_traces_node_id(self, step_id):
        g = self.graph(step_id)
        plt.figure(figsize=(15, 5))
        min_ts = min([g.get_node_scheduled_time(u) for u in g._V if u != 0 and u != 1])
        max_ts = min_ts + 80000
        segments = [(g.get_node_scheduled_time(u), g.get_node_finished_time(u), u) for u
                    in g._V if u != 0 and u != 1]
        # segments_sorted = sorted(segments, key=lambda x: x[0])
        segments_sorted = sorted(segments, key=lambda x: x[2])
        x = range(len(segments_sorted))
        ymin = [e[0] for e in segments_sorted]
        ymax = [e[1] for e in segments_sorted]
        colormap = ['blue', 'red', 'black', "yellow", "green"]
        device_set = {g._attribute[e[2]]['node_device'] for e in segments_sorted}
        colors = [colormap[self.device_name_to_id(g._attribute[e[2]]['node_device'])] for e in segments_sorted]
        plt.vlines(x, ymin, ymax, colors)
        patches = [mpatches.Patch(color=colormap[self.device_name_to_id(device)], label=device) for device in device_set]
        plt.legend(handles=patches, loc="upper center", bbox_to_anchor=(0.5, 1.2), ncol=3)
        plt.ylim(min_ts, max_ts)
        plt.show()

    def show_rendezvous_info(self, step_id):
        graph = self.graph(step_id)
        key_set = self.key_set(step_id)
        key_to_node_ids = self.key_to_node_ids(step_id)
        rendez_with_one_send = set()
        rendez_with_one_recv = set()
        rendez_same_device = set()
        for k in key_set:
            print("Key:", k[:-1])
            node_ids = list(key_to_node_ids[k])
            SP = k.split(';')
            k_send_device = SP[0]
            k_send_device_incarnation = int(SP[1], base=16)
            k_recv_device = SP[2]
            k_tensor_name = SP[3]
            frame_and_iter = SP[4].split(':')
            k_frame_id = int(frame_and_iter[0])
            k_iter_id = int(frame_and_iter[1])

            if len(node_ids) == 1:
                u = node_ids[0]
                if graph.is_send(u):
                    send_id = u
                    send_device = graph.get_node_device(send_id)
                    assert (send_device == k_send_device)
                    rendez_with_one_send.add(k)
                    print("Sender:", send_id, graph.get_node_device(send_id))
                elif graph.is_recv(u):
                    recv_id = u
                    rendez_with_one_recv.add(k)
                    recv_device = graph.get_node_device(recv_id)
                    assert (recv_device == k_recv_device)
                    print("Receiver:", recv_id, graph.get_node_device(recv_id))
                else:
                    assert(False)
            elif len(node_ids) == 2:
                if graph.is_send(node_ids[0]) and graph.is_recv(node_ids[1]):
                    send_id, recv_id = node_ids
                elif graph.is_send(node_ids[1]) and graph.is_recv(node_ids[0]):
                    recv_id, send_id = node_ids
                else:
                    assert(False)
                send_device = graph.get_node_device(send_id)
                assert(send_device == k_send_device)
                recv_device = graph.get_node_device(recv_id)
                assert (recv_device == k_recv_device)
                if send_device == recv_device:
                    rendez_same_device.add(k)
        print("In totally", len(key_set), "keys.")
        print("   Found",len(rendez_with_one_send),"with one send.")
        print("   Found", len(rendez_with_one_send), "with one recv.")
        print("   Found", len(rendez_same_device), "with the same device.")


def last_index(L):
    for i in range(len(L) - 1, 0, -1):
        if L[i] > 0:
            return i
    return -1


# traces = get_traces("/home/ybw/ZhuSuan/examples/rst/log.txt")
# traces = get_traces("./exp/log_single_vae.txt")
# traces = get_traces("./exp/log_1p1w_ipoib_vae.txt")
# traces = get_traces("./exp/log_1p1w_eth_vae.txt")

TestSet1 = [("./exp/log_single_vae.txt", (400, 400)), ("./exp/log_1p1w_ipoib_vae.txt", (400, 400)),
            ("./exp/log_1p1w_eth_vae.txt", (400, 400))]
TestSet2 = [("./exp/log_single_cifar10.txt", (21084, 21084)), ("./exp/log_1p1w_ipoib_cifar10.txt", (21083, 21083)),
            ("./exp/log_1p1w_eth_cifar10.txt", (21099, 21099))]
# TestSet = TestSet1 + TestSet2
#TestSet = [("/Volumes/ybw/performance_analysis_dag/exp_v1/log_single_vae.txt", (400, 420))]
TestSet = [("./exp_v3/log_single_resnet.txt", (400, 420))]
# TestSet = [("./exp/log_single_vgg.txt", (10,80))]
# TestSet = [("./exp/log_single_cifar10.txt", (21084,21084))]
# TestSet = TestSet1 + TestSet2
for test in TestSet:
    path = test[0]
    target_step_id_from = test[1][0]
    target_step_id_to = test[1][1]
    print(path)
    reader = TraceReader(lambda step_id: step_id in range(target_step_id_from, target_step_id_to + 1))
    reader.load(path)
    analysis = Analysis(reader)
    # g = analysis.step_id_to_graph[target_step_id]
    print("Read OK.")
    print("============== Step Information ====================")
    reader.show_runtime_num_traces_per_step()
    reader.show_duration_per_step_id()
    print("============== Basic Graph Information ====================")
    # Assertion Guarantee that ALL the graph in the target_step is the same.
    analysis.show_static_analysis(target_step_id_from)
    analysis.show_graph_num_nodes_per_device(target_step_id_from)
    analysis.show_graph_num_nodes_per_type(target_step_id_from)
    print()
    print("============== Scheduler Hole Analysis ====================")
    analysis.show_scheduler_hole_per_node(target_step_id_from)
    print("Summation of scheduler hole in critical path from _SINK / Total Duration")
    print("% 8s% 20s% 20s" % ("StepId", "SchedulerHole(us)", "Duration(us)"))
    print("============== Critical Path Analysis =====================")
    analysis.show_critical_path_node_device(target_step_id_from)

    #for step_id in range(target_step_id_from, target_step_id_to + 1):
    #    print("% 8d% 20f% 20f" % (
    #        step_id, analysis.get_critical_path_scheduler_hole(step_id), analysis.step_id_duration(step_id)))

        # analysis.show_graph_node_degrees_distribution(target_step_id)

        # analysis.simulate()

